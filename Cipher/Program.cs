﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cipher
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Caesar Cipher Message Encrypter");
            MainMenu();

        }

        static string GetMessage()
        {
            Console.WriteLine("Enter a message: ");
            string msg = Console.ReadLine();

            return msg;
        }

        static int GetShift()
        {
            Console.WriteLine("Enter a shift (1-25): ");
            string shiftStr = Console.ReadLine();
            int shift;

            while (!int.TryParse(shiftStr, out shift))
            {
                return GetShift();
            }

            return shift;
        }

        static void Encrypt()
        {
            int shift = GetShift();

            Caesar caesar = new Caesar(shift);

            string encrypted = caesar.Encipher(GetMessage());
            Console.WriteLine("Encrypted: " + encrypted);
        }

        static void Decrypt()
        {
            int shift = GetShift();

            Caesar caesar = new Caesar(shift);

            string decrypted = caesar.Decipher(GetMessage());
            Console.WriteLine("Decrypted: " + decrypted);
        }

        static void MainMenu()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine("1) Encrypt");
            Console.WriteLine("2) Decrypt");
            Console.WriteLine("3) Exit");
            Console.WriteLine("-----------------");

            Console.WriteLine("Select an item from menu: ");

            string selectionStr = Console.ReadLine();
            int selection;

            while (!int.TryParse(selectionStr, out selection) || !(selection < 1 || selection <= 3))
            {
                // Reprompt
                Console.WriteLine("Select an item from menu: ");
                selectionStr = Console.ReadLine();
            }

            switch (selection)
            {
                case 1:
                    Encrypt();
                    break;
                case 2:
                    Decrypt();
                    break;
                case 3:
                    Console.WriteLine("Bye!");
                    Environment.Exit(0);
                    break;
            }

            MainMenu();
        }
    }
}
