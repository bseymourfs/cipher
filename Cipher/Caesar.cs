﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cipher
{
    class Caesar
    {

        public int Shift {get; set;}

        private const string AlphabetUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string AlphabetLower = "abcdefghijklmnopqrstuvwxyz";

        public Caesar(int shift)
        {
            this.Shift = shift;
            
        }
        // Checks if a char is in our alphabet.
        private bool IsInAlphabet(char letter)
        {
            if (char.IsUpper(letter))
            {
                bool hasLetter = AlphabetUpper.IndexOf(letter) != -1;
                return hasLetter;
            } else if (char.IsLower(letter))
            {
                bool hasLetter = AlphabetLower.IndexOf(letter) != -1;
                return hasLetter;
            }

            return false;
        }
        // This method applies a + or - shift if the char is found in our alphabet.
        private char ShiftChar(char letter, int shift)
        {
            if (IsInAlphabet(letter))
            {
                // Apply shifts

                if (char.IsUpper(letter))
                {
                    int index = AlphabetUpper.IndexOf(letter);
                    int newIndex = index + shift;

                    // Wrap around if the new index is out of bounds.
                    if (newIndex >= AlphabetUpper.Length)
                    {
                        newIndex -= AlphabetUpper.Length;
                    }
                    else if (newIndex < 0)
                    {
                        // Out of bounds when decrypting
                        newIndex += AlphabetUpper.Length;
                    }

                    letter = AlphabetUpper[newIndex];

                } else if (char.IsLower(letter))
                {
                    int index = AlphabetLower.IndexOf(letter);
                    int newIndex = index + shift;

                    // Wrap around if the new index is out of bounds.
                    if (newIndex >= AlphabetLower.Length)
                    {
                        newIndex -= AlphabetLower.Length;
                    } else if (newIndex < 0)
                    {
                        // Out of bounds when decrypting
                        newIndex += AlphabetLower.Length;
                    }

                    letter = AlphabetLower[newIndex];
                }

            }

            return letter;
        }

        // Shifts the letters in a string
        public string ShiftString(string input, int shift)
        {
            char[] messageArray = input.ToCharArray();
            // Iterate over the array
            for (int i = 0; i < input.Length; i++)
            {
                char letter = messageArray[i];

                if (!IsInAlphabet(letter))
                    continue; // Skip
                else
                {
                    // Replace with the shifted letter
                    messageArray[i] = ShiftChar(letter, shift);
                }
            }

            return new string(messageArray);
        }

        public string Encipher(string message)
        {

            return ShiftString(message, this.Shift);

        }

        public string Decipher(string message)
        {
            // Apply the reverse shift (which is negative)
            return ShiftString(message, -this.Shift);

        }

    }
}
